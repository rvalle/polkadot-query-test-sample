import { Keyring } from '@polkadot/keyring';
import { ApiPromise, WsProvider } from '@polkadot/api';

import {expect,should,assert} from 'chai';


// TODO: Copy test address as returned from extension API
const address="5EF.....";
const endpoint='wss://rpc.polkadot.io';

describe("Address Transformations",function(){
    it("Decoding an Address",function(){
        const keyring = new Keyring();
        const pub_addr=keyring.decodeAddress(address);
        console.log(pub_addr);
        assert(pub_addr.length===32);
        console.log(keyring.encodeAddress(pub_addr,0));
    })
});

describe("Chain Interaction",function(){
    let api;

    before("Will Create the API ", async function(){
        const wsProvider = new WsProvider(endpoint);
        api = await ApiPromise.create({ provider: wsProvider });
    });

    it("Will check information", async function(){
        // get the chain information
        const chainInfo = await api.registry.getChainProperties();
        assert(chainInfo.has('ss58Format'));
        console.log(chainInfo);
    });

    it("Will check account balance",async function(){
        // Retrieve the last timestamp
        const now = await api.query.timestamp.now();
        // Retrieve the account balance & nonce via the system module
        const { nonce, data: balance } = await api.query.system.account(address);
        assert(balance.has('free'));
        console.log(`${now}: balance of ${balance.free} and a nonce of ${nonce}`);
    });

    it("Will check max nominators", function() {
        const max_nom=api.consts.staking.maxNominations.toNumber();
        assert(max_nom>0);
    });

    it("Will query the current nominated validators", async function(){
        const result = await api.query.staking.nominators(address);
        assert(result.isSome);
        console.log(JSON.stringify(result.toHuman().targets));
    });

});

